<?php
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "exceptions/QueryException.php";
require_once "entity/Usuario.php";
require_once "core/App.php";
require_once "core/bootstrap.php";


// Este controlador de login comprueba que un usuario existe con esas credenciales, de lo contrario, mostrara errores por pantalla mediante excepciones
try {
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        // Comprobacion de los datos recibidos por formulario y manejo de errores
        if (!empty($_POST['email'])) {
            $email = trim(htmlspecialchars($_POST['email']));
        }
        else {
            throw new Exception("El campo email es obligatorio");
        }
        if (!empty($_POST['password'])) {
            $password = md5(trim(htmlspecialchars($_POST['password'])));
        }
        else {
            throw new Exception("El campo contraseña es obligatorio");
        }

        $con = new QueryBuilder('usuarios', "Usuario");
        $user = $con->findUser($email);

        if($user->getPassword() == $password) {
            header('Location: /blograul/admin'); // En caso de login correcto, redirige a Google
        }
    }
}
catch (Exception $loginExcepcion){
    $errores[] = $loginExcepcion->getMessage();
}
require_once __DIR__ . "/../app/views/partials/login.part.php";
//require "views/partials/login.part.php";
?>