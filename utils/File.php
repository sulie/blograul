<?php

// Clase que instancia objetos de tipo fichero, y los guarda en el servidor
class File {
    private $file; 
    private $fileName;

    public function __construct(string $fileName, array $arrTypes) {

        $this->file = $_FILES[$fileName]; // Almacenamos el fichero completo en una variable file, que se comportará como array asociativo
        $this->fileName = $this->file["name"]; // Almacenamos el nombre del fichero en una variable "nombre"

        // Comprueba que se haya mandado una imagen
        if ($this->file["name"] == "") {
            throw new FileException("No se ha mandado el archivo");
        }

        // Comprueba si existen errores de tamaño, subida parcial y otros problemas
        if ($this->file["error"] !== UPLOAD_ERR_OK) {
            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException("El archivo es demasiado grande");

                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("El archivo no se ha subido completamente");

                default:
                    throw new FileException("Error en la subida del archivo");
                    break;
            }            
        }

        // Comprueba que se haya mandado un archivo del tipo especificado y no otro
        if (in_array($this->file["type"], $arrTypes)===false) {
            throw new FileException("El archivo no es válido");
        }            
    }

    // Getter de fileName
    public function getFileName() {
        return $this->fileName;
    }

    // Guarda en el servidor
    public function saveUploadFile(string $ruta) {
        if (is_uploaded_file($this->file["tmp_name"])) { // Comprueba que se ha subido correctamente la imagen

            if (is_file($ruta.$this->file["name"])) { // Comprueba que no existe un fichero con ese nombre, si existe, lo renombra con un id unico basado en el tiempo
                $idUnico = time();
                $this->file["name"] = $idUnico."_".$this->file["name"];
                $this->fileName = $this->file['name'];
            }
            // Lanza una excepción si no se pudo mover el archivo de la carpeta temporal a la ruta deseada
            if (move_uploaded_file($this->file["tmp_name"],$ruta.$this->file["name"]) == false){
                throw new FileException("El archivo no se ha podido mover al destino especificado");
            }
        }
        else {
            throw new FileException("El archivo no se ha subido mediante formulario");
        }
    }

    // Esta función no se usa en esta aplicación, viene del proyecto de las actividades en clase
    public function copyFile($origin,$destiny){
        if (!is_file($origin.$this->file["name"])) {
            throw new FileException("No existe el archivo ".$origin.$this->file["name"]);
        }
        else if (is_file($destiny.$this->file["name"])){
            throw new FileException("El archivo ".$destiny.$this->file["name"]." ya existe");
        }
        else if (!copy($origin.$this->file["name"], $destiny.$this->file["name"])){
            throw new FileException("No se ha podido copiar el archivo");
        }
    }
}
?>