<?php

// Función para acortar el contenido de una entrada en X palabras y mostrarlo así en las preview de las entradas
function blogPreview ($content, $limit) {
    if (strlen($content) > $limit) {
        $corte = explode(" ",$content, $limit); // Almacena en un array las palabras cortadas
        $res = ""; // Almacena el resultado de la preview
    
        unset($corte[count($corte)-1]); // Elimina el sobrante de la cadena (el resto de palabras que no deseas que se muestren)
        
        return $res = implode(" ",$corte); // Une las palabras que se desean mostrar en una cadena y las devuelve
    }
    else {
        return $content;
    }
}

// Esta función no se usa en esta aplicación porque la plantilla no lo necesita
function activePage($page) {
    if($page == $_SERVER['REQUEST_URI']){
        return true;
    }
    else{
        return false;
    }
}
?>