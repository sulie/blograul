<?php 
require_once "App.php"; // Para la conexión con la BBDD
require_once "Request.php"; // Para que index.php que va a usar bootstrap.php pueda acceder al metodo Request::uri
$config = require_once __DIR__ . "/../app/config.php"; // Configuración de la BBDD
App::bind("config", $config); // Almacenar la configuración de la BBDD en el contenedor de App
?>