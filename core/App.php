<?php
require_once __DIR__."/../exceptions/AppException.php";

// Esta clase se encarga de almacenar en un contenedor diferentes valores y configuraciones, como por ejemplo la conexión con la BBDD
class App {
    private static $container = [];

    // Setter para añadir claves y valores al array $container
    public static function bind (string $key, $value) { 
        static::$container[$key] = $value;
    }

    // Getter que obtiene el valor del array $container, según una clave especificada por parámetro
    public static function get (string $key) {
        if (array_key_exists($key, static::$container)) {
            return static::$container[$key];
        }
        else {
            throw new AppException("No se ha encontrado la clave $key en el contenedor.");
        }
    }

    // Obtiene los parámetros de conexión y si no existe, la crea
    public static function getConnection() {
        
        if (!array_key_exists("connection", static::$container)) {
            static::$container["connection"] = Connection::make(); // Usa el método make() de database/Connection.php
            
        }
        
        return static::$container["connection"];
    }
}
