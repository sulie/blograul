<?php
class Request {
    // Devuele una uri a partir de una url, es imprescindible para las rutas amigables usando parámetros por GET
    public static function uri(){
        return trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");
    }
}
?>