<?php 
require "core/bootstrap.php"; // Obtiene conexiones con la BBDD y la clase Request para usar Request::uri()
$routes = require "app/routes.php"; // Almacena las rutas de la aplicación

require $routes[Request::uri()]; // Importa el controlador que se necesite según la ruta que hay en la URI 
?>