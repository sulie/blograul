<?php
interface IEntity {
    // Interfaz que define que quien la implementa (entidades), ha de tener un método que pase de objeto a array
    public function toArray():array;
}
?>