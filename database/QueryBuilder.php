<?php
require_once __DIR__ ."/../core/App.php";
require_once __DIR__ ."/../database/IEntity.php";
require_once __DIR__ ."/../exceptions/QueryException.php";

/*
Esta clase se encarga de construir consultas SQL y ejecutarlas, estas consultas son las típicas:
    Encontrar todos los elementos de una tabla
    Encontrar un elemento de una tabla (y su variante para los usuarios, lo cual también se puede resolver con un repositorio)
    Borrar un elemento de una tabla
    Insertar un elemento en una tabla
    Actualizar un elemento en una tabla
*/
class QueryBuilder {

    private $connection;
    private $table;
    private $classEntity;

    // El constructor almacena la conexión con la BBDD mediante el método App::getConnection
    public function __construct(string $table, string $classEntity) {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    // Ejecuta una sentencia SQL pasada por parámetro
    public function executeQuery(string $sql): array {

        $pdoStatement = $this->connection->prepare($sql);
        if ($pdoStatement->execute()===false)
        throw new QueryException("No se ha podido ejecutar la consulta");
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity); 
    }

    // Encuentra todos los elementos de la tabla del objeto QueryBuilder instanciado (posiblemente como repositorio de una entidad)
    public function findAll() {
        $sql = "SELECT * FROM $this->table ORDER BY id desc";
        return $this->executeQuery($sql);
    }

    // Encuentra un elemento de la tabla del objeto QueryBuilder instanciado (posiblemente como repositorio de una entidad)
    public function findById(string $id) {
        $sql = "SELECT * FROM $this->table WHERE id = :id";
        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute(array(':id' => $id)) === false) throw new QueryException("No se ha podido ejecutar la consulta", 1);
        
        return $pdoStatement->fetchObject($this->classEntity);
    }

    // Borra un elemento de la tabla del objeto QueryBuilder instanciado (posiblemente como repositorio de una entidad)
    public function deleteById(string $id) {
        $sql = "DELETE FROM $this->table WHERE id = :id";
        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute(array(':id' => $id)) === false) throw new QueryException("No se ha podido ejecutar la consulta", 1);
    }

    // Encuentra un usuario de la tabla usuario (se puede hacer con repositorio también)
    public function findUser(string $email) {
        $sql = "SELECT * FROM $this->table WHERE email = '$email'";
        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute() === false) throw new QueryException("Error en la validación del usuario", 1);

        return $pdoStatement->fetchObject($this->classEntity);
    }

    // Variante de findById sin sentencia preparada PDO y usando executeQuery
    public function find (int $id): IEntity {
        $sql = "SELECT * FROM $this->table WHERE id=$id";
        $result = $this->executeQuery($sql);

        if (empty($result)) {
            throw new NotFoundException("No se ha encontrado el elemento con id $id");
        }

        return $result[0]; 
    }

    // Insertar objetos en la base de datos
    public function save(IEntity $entity): void {
        try {
            $parameters = $entity->toArray(); // Pasar de objeto a array
            $sql = sprintf("insert into %s (%s) values (%s)", // Sentencia preparada PDO

            $this->table, // Esto va en la primera "%s" y es la tabla del QueryBuilder instanciado
            implode(", ", array_keys($parameters)), // Esto va en la segunda "%s" y es el nombre de los campos separados por comas
            // Esto va en la tercera "%s" y es el nombre de los campos separados por comas y empezando por ":" para realizar la sentencia preparada PDO
            ":". implode(", :", array_keys($parameters)) 
            );

            $statement = $this->connection->prepare($sql); // Prepara la consulta
            $statement->execute($parameters); // Ejecuta la consulta

        } 
        catch (PDOException $exception) {
            throw new QueryException("Error al insertar en la BBDD.");
        } 
    }

    public function updateById(string $id, array $parameters) {
        try {
            $toUpdate = $this->findById($id); // Instancia del objeto a actualizar
            $propiedades = $toUpdate->toArray(); // Paso de propiedades a array
            $campos = [];

            // Construccion de la sentencia preparada PDO
            foreach ($propiedades as $propiedad => $valor) {
                //array_push($campos,$propiedad);
                if ($propiedad != "id" && $propiedad != "fecha")
                $campos[] = "$propiedad = :$propiedad";
            }

            $sql = sprintf("UPDATE %s SET %s WHERE id = $id",
        
            $this->table,
            implode(", ",$campos)
            );

            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        }
        catch (PDOException $exception) {
            unlink($parameters[2]);
            throw new QueryException($exception."Error al insertar en la BBDD.".$parameters[2]);
        } 
    }
}