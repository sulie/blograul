<?php
class Connection {
    // Crea una nueva conexión PDO con la BBDD mediante la configuración recibida de App::get, que a su vez viene de config.php
    public static function make() {
        try {
            $config = App::get("config")["database"];
            $pdo = new PDO($config['connection'].'dbname='.$config['name'], $config['username'], $config['password'], $config['options']);
        }
        catch (PDOException $PDOException) {
            throw new AppException("No se ha podido establecer la conexión con la base de datos");
        }
        return $pdo;
    }
}
?>