<?php
// Un repositorio es una extensión de QueryBuilder, pero construyendo el objeto por defecto con una tabla y entidad predefinidas, ahorra tiempo a la hora de usar QueryBuilder
class CategoriaRepository extends QueryBuilder {

    public function __construct(string $table="categorias", string $classEntity="Categoria") {
        parent::__construct($table, $classEntity);
    }
}
?>