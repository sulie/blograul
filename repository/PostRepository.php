<?php
// Un repositorio es una extensión de QueryBuilder, pero construyendo el objeto por defecto con una tabla y entidad predefinidas, ahorra tiempo a la hora de usar QueryBuilder
class PostRepository extends QueryBuilder {

    public function __construct(string $table="entradas", string $classEntity="Post") {
        parent::__construct($table, $classEntity);
    }

    // Esta función obtiene la categoría de un Post en concreto y la devuelve
    public function getCategoria(Post $post): Categoria {

        $categoriaRepository = new CategoriaRepository();
        return $categoriaRepository->find($post->getCategoria()); // Usamos la variante find de findById
    }
}
?>