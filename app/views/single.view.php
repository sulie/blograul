<?php include "partials/head.part.php";?>
                 <!-- Blog Post (Right Sidebar) Start -->
                 <div class="col-md-9">
                    <div class="col-md-12 page-body">
                    	<div class="row">
                    		
                            <div class="sub-title">
                           		<a href="index.php"><h1>Volver</h1></a>
                             </div>
                            
                            
                            <div class="col-md-12 content-page">
                              <div class="col-md-12 blog-post">
                            	
                                
                                <!-- Post Headline Start -->
                                <div class="portada">
                                    <img src="<?= $post->getPortada() ?>" alt="Portada" class="img-fluid">
                                </div>
                                <div class="post-title">
                                    <h2><?= $post->getTitulo() ?></h2> 
                                </div>
                                <!-- Post Headline End -->
                                    
                                    
                                <!-- Post Detail Start -->
                                <div class="post-info">
                                    <span><?= $post->getFecha() ?></span>
                                </div>
                                <!-- Post Detail End -->
                                    
                                    
                                <p><?= $post->getContenido(); ?></p>                                        
                                </div>    
                             </div>
                              
                         </div>
<?php include "partials/subscribe.part.php"; ?>             
                         </div>
<?php include "partials/footer.part.php"; ?>