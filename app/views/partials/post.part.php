<?php
foreach ($posts as $entrada) {
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-12">
        <img class="img-fluid" src=<?= $entrada->getPortada() ?>>
      </div>

      <div class="col-md-8 col-12">
        <h2><?= $entrada->getTitulo() ?></h2>
        <p><?= blogPreview($entrada->getContenido(), 35)."..." ?></p>
        <p><?= "Categoría: ".$postRepository->getCategoria($entrada)->getNombre() ?></p>
        <p class="text-left"><a href="single?id=<?= $entrada->getId() ?>">Leer más</a></p>
        <p class="text-right"><?= $entrada->getFecha() ?></p>
      </div>
    </div>
  </div>

  <hr>
  <br>
  <?php
}
?>