<!-- Footer Start -->
<div class="col-md-12 page-body margin-top-50 footer">
    <footer>
        <ul class="menu-link">
            <li><a href="index.html">Inicio</a></li>
            <li><a href="about.html">Info</a></li>
            <li><a href="work.html">Trabajo</a></li>
            <li><a href="contact.html">Contacto</a></li>
        </ul>

        <p>© Copyright 2016 DevBlog. All rights reserved</p>


        <!-- UiPasta Credit Start -->
        <div class="uipasta-credit">Design By <a href="https://www.uipasta.com" target="_blank">UiPasta</a></div>
        <!-- UiPasta Credit End -->


    </footer>
</div>
<!-- Footer End -->


</div>
<!-- Blog Post (Right Sidebar) End -->

</div>
</div>
</div>



<!-- Back to Top Start -->
<a href="#" class="scroll-to-top"><i class="fa fa-long-arrow-up"></i></a>
<!-- Back to Top End -->


<!-- All Javascript Plugins  -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/plugin.js"></script>

<!-- Main Javascript File  -->
<script type="text/javascript" src="js/scripts.js"></script>


</body>

</html>