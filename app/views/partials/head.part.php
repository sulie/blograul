<!DOCTYPE html>
<html lang="es">

<head>

  <!-- Meta Tag -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Raul Dev's</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="images/favicon/favicon.ico">
  <link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/apple-touch-icon.png">

  <!-- All CSS Plugins -->
  <link rel="stylesheet" type="text/css" href="css/plugin.css">

  <!-- Main CSS Stylesheet -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Google Web Fonts  -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">


  <!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
  <!--[if lt IE 9]>
	   <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  <!-- Preloader Start -->
  <div class="preloader">
    <div class="rounder"></div>
  </div>
  <!-- Preloader End -->
  <div id="main">
    <div class="container">
      <div class="row">

        <!-- About Me (Left Sidebar) Start -->
        <div class="col-md-3">
          <div class="about-fixed">

            <div class="my-pic">
              <img src="images/pic/my-pic.png" alt="">
              <a href="javascript:void(0)" class="collapsed" data-target="#menu" data-toggle="collapse"><i
                  class="icon-menu menu"></i></a>
              <div id="menu" class="collapse">
                <ul class="menu-link">
                  <li><a href="/blograul">Inicio</a></li>
                  <li><a href="about">Info</a></li>
                  <li><a href="work">Trabajos</a></li>
                  <li><a href="contact">Contacto</a></li>
                </ul>
                <p class="text-center"><a href="login">Log In</a></p>
              </div>
            </div>



            <div class="my-detail">

              <div class="white-spacing">
                <span>Raúl Santana Tilves</span>
                <span class="my-sub-title">Desarrollador Web</span>
              </div>

              <ul class="social-icon">
                <li><a href="#" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" target="_blank" class="github"><i class="fa fa-github"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
        <!-- About Me (Left Sidebar) End -->