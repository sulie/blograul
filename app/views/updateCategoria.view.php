<!DOCTYPE html>
<html lang="es">

<head>

    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Raul Dev's Admin</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/apple-touch-icon.png">


    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    

    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <link rel="stylesheet" type="text/css" href="css/admin.css">


    <!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	   <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <header class="bg-dark py-2 mb-4">
        <h1 class="display-5 text-center text-light">Modificar categoria</h1>
    </header>

    <div class="container bg-dark rounded py-3 mb-4">
        <form method='POST' enctype="multipart/form-data" action="updateCategoria?id=<?= $toUpdate->getId() ?>">
            <div class="form-group">
                <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?= $toUpdate->getNombre() ?>">
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>

    <div class="container mb-5">
        <a class="btn btn-success mb-5" href="admin" role="button">Volver</a>
    </div>
    <footer class="fixed-bottom bg-dark py-2 mt-4">
        <p class="text-light text-center">Raúl Santana Tilves</p>
    </footer>
</body>

</html>