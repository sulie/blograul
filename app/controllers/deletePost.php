<?php
require_once "entity/Post.php";
require_once "entity/Categoria.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "core/bootstrap.php";

// Simple controlador que toma un id mandado por POST mediante un formulario con un campo tipo hidden, en la tabla de entradas, aparentemente como un botón de borrado.
try {
    $del = new QueryBuilder('entradas','Post');

    $toDelete = $del->findById($_POST['id']);
    $delPortada = $toDelete->getPortada();

    unlink($delPortada);
    $del->deleteById($_POST['id']);
    header('Location: admin');
}
catch (PDOException $PDOException) {
    $errores[] = $PDOException->getMessage();
}

?>