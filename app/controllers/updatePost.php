<?php
require_once "entity/Post.php";
require_once "entity/Categoria.php";
require_once "core/App.php";
require_once "utils/utils.php";
require_once "utils/File.php";
require_once "exceptions/FileException.php";
require_once "exceptions/AppException.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "repository/PostRepository.php";
require_once "repository/CategoriaRepository.php";
require_once "core/bootstrap.php";

$errores = array();
$titulo = "";
$contenido = "";
$categoria = "";


/*
Este controlador se encarga de actualizar una entrada por id, el cual se recibe de un enlace mediante GET, que se encuentra en la tabla de entradas,
Hace uso del repositorio para encontrar el elemento (findById), modificar el objeto mediante un array de uno o más valores y luego lo actualiza en la base de datos
*/
try {

    $postRepository = new PostRepository();
    $categoriaRepository = new CategoriaRepository();
    $categorias = $categoriaRepository->findAll(); // Encuentra todas las categorías para mostrarlas en options del select del formulario de edición

    $toUpdate = $postRepository->findById($_GET['id']); // Encuentra el objeto Post a actualizar

    // Almacenamos la ruta de la portada sin modificar, para que en caso de que no se actualice se mantenga, y si se actualiza, la borramos del servidor
    $oldPortada = $toUpdate->getPortada();

    // Almacenamos la categoría anterior para que se muestre como "selected" en el select del formulario y el usuario tenga claro que categoría poseía antes de la edición
    $oldCategoria = $toUpdate->getCategoria();
    
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        // Bloque que almacena los datos de tipo texto del formulario
        $titulo = trim(htmlspecialchars($_POST["titulo"]));
        $contenido = nl2br(trim(htmlspecialchars($_POST["contenido"])));
        $categoria = trim(htmlspecialchars($_POST["categoria"]));

        /* 
        Importante: Esta parte se encarga de comprobar si se ha mandado una imagen por el formulario o no.
        Si se ha mandado imagen, se entiende que se quiere actualizar la portada, por lo que instanciamos un nuevo File de tipo image y lo guardamos en el servidor,
        posteriormente se borra la antigua portado mediante unlink y se le asgina a la variable rutaPortada el contenido: Constante de la ruta + nombre de archivo de la portada

        Si no se ha mandado imagen, se entiende que NO se quiere actualizar la portada y se desea mantenerla, por lo que tan solo asignaremos a la variable rutaPortada
        el valor de la anterior portada, almacenada al principio del controlador por este mismo motivo.

        Es importante diferenciar las siguiente variables:
            $portada: Objeto de tipo File que SOLO se usa si se va a actualizar la portada.
            $oldPortada: String que contiene la ruta completa de la portada antes de actualizar.
            $rutaPortada: Variable que se usará como valor para actualizar el campo ´categoria´ dentro de la BBDD, si se actualiza la portada, se almacena la ruta nueva,
            si no, se almacena la que ya estaba y se mantiene.
        */
        if ($_FILES['portada']['name'] != null) { // Detecta si hay portada nueva, es decir, no es null
            $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
            $portada = new File("portada", $tiposAceptados); // Instanciamos la nueva portada
            $portada->saveUploadFile(Post::RUTA_IMAGENES_POST); // Guardamos en servidor

            unlink($oldPortada); // Borramos la imagen anterior del servidor
            $rutaPortada = Post::RUTA_IMAGENES_POST.$portada->getFileName(); // Campo portada = constante de la ruta + nombre de archivo de la nueva portada.
        }
        else { // No hay portada nueva, se mantiene la ruta con la portada anterior
            $rutaPortada = $oldPortada;
        }

        // Campos de la base de datos como clave, valores a actualizar como valores.
        $valores = [
            "titulo" => $titulo,
            "contenido" => $contenido,
            "portada" => $rutaPortada, // Aquí entra en juego la variable más importante del controlador
            "categoria" => $categoria
        ];

        $postRepository->updateById($_GET['id'],$valores); // Actualizamos el objeto en la base de datos

        header('Location: admin'); // Redirigimos al panel de administración
    }
}
catch (AppException $appException) {
    array_push($errores, $appException->getMessage());
}

catch (FileException $fileException) {
    array_push($errores, $fileException->getMessage());
}
require_once __DIR__ . "/../views/updatePost.view.php";
?>