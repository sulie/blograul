<?php
require_once "entity/Post.php";
require_once "entity/Categoria.php";
require_once "core/App.php";
require_once "utils/utils.php";
require_once "exceptions/AppException.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "repository/CategoriaRepository.php";
require_once "core/bootstrap.php";

$errores = array();

/*
Este controlador se encarga de actualizar una categoria por id, el cual se recibe de un enlace mediante GET, que se encuentra en la tabla de categorías,
Hace uso del repositorio para encontrar el elemento (findById), modificar el objeto mediante un array de uno o más valores y luego lo actualiza en la base de datos
*/
try {
    $categoriaRepository = new CategoriaRepository();
    $toUpdate = $categoriaRepository->findById($_GET['id']); // Encuentra el objeto
    
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $nombre = trim(htmlspecialchars($_POST["nombre"])); // Almacena los datos recibidos por formulario

        // Asociamos los valores a sus claves
        $valores = [
            "nombre" => $nombre
        ];

        $categoriaRepository->updateById($_GET['id'],$valores); // Actualiza

        header('Location: categorias'); // Redirige a la administración de categorías
    }
}
catch (AppException $appException) {
    array_push($errores, $appException->getMessage());
}

require_once __DIR__ . "/../views/updateCategoria.view.php";
?>