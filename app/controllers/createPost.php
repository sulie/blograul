<?php
require_once "entity/Post.php";
require_once "entity/Categoria.php";
require_once "utils/utils.php";
require_once "utils/File.php";
require_once "exceptions/FileException.php";
require_once "exceptions/AppException.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "repository/PostRepository.php";
require_once "repository/CategoriaRepository.php";
require_once "core/bootstrap.php";

$errores = array();
$titulo = "";
$contenido = "";
$portada = "";
$categoria = "";

/*
Este controlador se encarga de crear nuevas entradas, recibiendo datos de un formulario, con los que se instancia un objeto de tipo Post,
luego se almacena en la base de datos usando el metodo save de PostRepository, que hereda de QueryBuilder.

También se hace uso del repositorio de las categorías, ya que ambas entidades están relacionadas, y se necesita que, al insertar una entrada,
se le asigne una categoría válida y existente en la base de datos.
*/

try {
    $postRepository = new PostRepository();
    $categoriaRepository = new CategoriaRepository();
    $categorias = $categoriaRepository->findAll(); // Se usa para mostrar las categorias en un campo select en el formulario de creación

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        // Este bloque almacena los campos del formulario de tipo texto, a excepción de la categoría, que es un option, pero lo que se recibe sigue siendo un value con texto
        $titulo = trim(htmlspecialchars($_POST["titulo"]));
        $contenido = nl2br(trim(htmlspecialchars($_POST["contenido"])));
        $categoria = trim(htmlspecialchars($_POST["categoria"]));

        // Este bloque se encarga de la portada, la cual ha de ser de uno de esos tres tipo de imagen, para luego instanciarse una imagen y guardarla en el servidor
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $portada = new File("portada", $tiposAceptados);
        $portada->saveUploadFile(Post::RUTA_IMAGENES_POST);

        // Este bloque se encarga de instanciar en sí el objeto Post que será almacenado en la base de datos con PDO, en mi caso, las entidades no pueden tener constructor,
        // ya que existe conflicto con el método fetchObject de PDO, por lo que la clase usa setters para realizar la función de constructor, funciona de igual forma.
        $entrada = new Post();
        $entrada->setTitulo($titulo);
        $entrada->setContenido($contenido);
        $entrada->setPortada(Post::RUTA_IMAGENES_POST.$portada->getFileName());
        $entrada->setFecha(date('Y-m-d H:i:s'));
        $entrada->setCategoria($categoria);

        // Guarda en la base de datos (método save de PostRepository heredado de QueryBuilder)
        $postRepository->save($entrada);

        $mensaje = "Publicación creada";
    }
}
catch (AppException $appException) {
    array_push($errores, $appException->getMessage());
}

catch (FileException $fileException) {
    array_push($errores, $fileException->getMessage());
}
require_once __DIR__ . "/../views/createPost.view.php";
?>