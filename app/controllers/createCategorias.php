<?php
require_once "entity/Categoria.php";
require_once "utils/utils.php";
require_once "utils/File.php";
require_once "exceptions/FileException.php";
require_once "exceptions/AppException.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "repository/CategoriaRepository.php";
require_once "core/bootstrap.php";

$errores = array();
$nombre = "";


/*
Este controlador se encarga de crear nuevas categorias, recibiendo datos de un formulario, con los que se instancia un objeto de tipo Categoria,
luego se almacena en la base de datos usando el metodo save de CategoriaRepository, que hereda de QueryBuilder.
*/
try {
    $categoriaRepository = new CategoriaRepository();
    $categorias = $categoriaRepository->findAll();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $nombre = trim(htmlspecialchars($_POST["nombre"]));

        $categoria = new Categoria();
        $categoria->setNombre($nombre);

        $categoriaRepository->save($categoria);

        $mensaje = "Categoria creada";
    }
}
catch (AppException $appException) {
    array_push($errores, $appException->getMessage());
}

catch (FileException $fileException) {
    array_push($errores, $fileException->getMessage());
}
require_once __DIR__ . "/../views/createCategoria.view.php";
?>