<?php
require_once "entity/Categoria.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "core/bootstrap.php";

// Simple controlador que toma un id mandado por POST mediante un formulario con un campo tipo hidden, en la tabla de categorías, aparentemente como un botón de borrado.
try {
    $del = new QueryBuilder('categorias','Categoria');

    $del->deleteById($_POST['id']);
    header('Location: categorias');
}
catch (PDOException $PDOException) {
    $errores[] = $PDOException->getMessage();
}

?>