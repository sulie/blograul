<?php 
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "entity/Post.php";
require_once "entity/Categoria.php";
require_once "utils/utils.php";
require_once "repository/PostRepository.php";
require_once "repository/CategoriaRepository.php";
require_once "core/bootstrap.php";


/*
Este controlador es simple, usa los repositorios para obtener los elementos guardados en la base de datos y los almacena en un array de objetos "Post",
luego se muestran en la vista correspondiente mediante un forEach que lo recorre.
*/
try {
    $postRepository = new PostRepository();
    $categoriaRepository = new CategoriaRepository();

    $posts = $postRepository->findAll();
}
catch (PDOException $PDOException) {
    $errores[] = $PDOException->getMessage();
}
require __DIR__ . "/../views/index.view.php";
?>