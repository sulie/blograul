<?php
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "entity/Post.php";
require_once "entity/Categoria.php";
require_once "utils/utils.php";
require_once "repository/PostRepository.php";
require_once "repository/CategoriaRepository.php";
require_once "core/bootstrap.php";

/*
Esta parte del codigo encuentra todos los elementos en la base de datos sobre categorias.
El objetivo es mostrarlos en una tabla, para posteriormente poder editar y borrar esos contenidos.
El motivo por el cual controlador y vista se encuentran en el mismo archivo es que es un controlador muy pequeño, y de esta forma tengo menos confusión con los ficheros,
he respetado el orden de todas formas, colocando el controlador al principio del documento y usandolo en la vista solo en los lugares necesarios.
*/
try {
    $categoriaRepository = new CategoriaRepository();
    $categorias = $categoriaRepository->findAll();
}
catch (PDOException $PDOException) {
    $errores[] = $PDOException->getMessage();
}
?>
<html lang="es">

<head>

    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Raul Dev's Admin</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/apple-touch-icon.png">

    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <link rel="stylesheet" type="text/css" href="css/admin.css">

    <!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
    <header class="bg-dark p-2 mb-4">
       <h1 class="display-5 text-center text-light">Administrar entradas</h1>
    </header>
    <div class="container">
        <a class="btn btn-primary mb-4" href="createCategoria" role="button">Crear nueva categoria</a>

        <table class="table table-striped table-responsive-md table-hover table-light">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#Id</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Accion</th>
                </tr>
            </thead>

            <?php
            foreach ($categorias as $tupla) {
            ?>
            <tr>
                <td><a class="btn btn-primary" href="updateCategoria?id=<?= $tupla->getId() ?>" role="button"><?= $tupla->getId() ?></a></td>
                <td><?= $tupla->getNombre(); ?></td>
                <td>
                    <form action="deleteCategoria" method="POST">
                        <input type="hidden" name="id" value="<?= $tupla->getId(); ?>">
                        <button type="submit" class="btn btn-danger">Borrar</button>
                    </form>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>
    <div class="container">
        <a class="btn btn-success" href="admin" role="button">Volver</a>
    </div>
    <footer class="fixed-bottom bg-dark py-2 mt-4">
        <p class="text-light text-center">Raúl Santana Tilves</p>
    </footer>
</body>

</html>