﻿<?php 
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "entity/Post.php";
require_once "core/bootstrap.php";

// Este controlador toma el id de una entrada mandado por el enlace "Leer más" y lo encuentra en la base de datos, luego lo muestra en la vista.
try {
    $con = new QueryBuilder('entradas','Post');
    $post = $con->findById($_GET['id']);
}

catch (PDOException $PDOExcepcion) {
    $errores[] = $PDOExcepcion->getMessage();
}
require __DIR__ . "/../views/single.view.php"; 
?>