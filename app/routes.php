<?php 
// Rutas del MVC, que luego usará el controlador principal /index.php, aquí se encuentran todas las rutas amigables de la aplicación
return [

    "blograul" => "app/controllers/index.php",
    "blograul/about" => "app/controllers/about.php",
    "blograul/admin" => "app/controllers/admin.php",
    "blograul/login" => "login/index.php",
    "blograul/contact" => "app/controllers/contact.php",
    "blograul/createPost" => "app/controllers/createPost.php",
    "blograul/deletePost" => "app/controllers/deletePost.php",
    "blograul/updatePost" => "app/controllers/updatePost.php",
    "blograul/categorias" => "app/controllers/categorias.php",
    "blograul/createCategoria" => "app/controllers/createCategorias.php",
    "blograul/deleteCategoria" => "app/controllers/deleteCategorias.php",
    "blograul/updateCategoria" => "app/controllers/updateCategorias.php",
    "blograul/single" => "app/controllers/single.php",
    "blograul/work" => "app/controllers/work.php"
]

?>